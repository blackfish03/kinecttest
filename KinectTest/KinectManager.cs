﻿using Microsoft.Kinect;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;

namespace KinectTest
{
    public class KinectManager
    {
        private KinectSensor kinectSensor;
        private MultiSourceFrameReader reader;
        private ColorFrameReader colorFrameReader;

        public List<Bitmap> imageList;
        public List<Body> bodyframeList = new List<Body>();
        public List<Tuple<Bitmap, TimeSpan>> depthBitmapsAndTimeStampsList = new List<Tuple<Bitmap, TimeSpan>>();

        public List<Bitmap> depthList = new List<Bitmap>();
        private ushort[] depthPixelsUShortArray;
        private byte[] depthPixelsByteArray;
        IList<Body> bodies = null;
        Stopwatch colorStopwatch = new Stopwatch();
        Stopwatch bodyStopwatch = new Stopwatch();
        public TimeSpan span = new TimeSpan();
        ushort[] depthArray;
        //load kinect sensor
        public bool LoadKinectSensor()
        {
            kinectSensor = KinectSensor.GetDefault();
            if (kinectSensor != null)
            {
                kinectSensor.Open();
                Thread.Sleep(2000);
            }
            return kinectSensor.IsAvailable;

        }


        public void StartRecordingStreams()
        {
           
            depthArray = new ushort[kinectSensor.DepthFrameSource.FrameDescription.Width * kinectSensor.DepthFrameSource.FrameDescription.Height];
            imageList = new List<Bitmap>();
            reader = kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body | FrameSourceTypes.Depth );
            //colorFrameReader = kinectSensor.ColorFrameSource.OpenReader();
            reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            //colorFrameReader.FrameArrived += FrameReader_FrameArrived;



        }

        public void CloseKinectSensor()
        {
            kinectSensor.Close();
        }

        public void StopRecording()
        {
            colorStopwatch.Stop();
            span = colorStopwatch.Elapsed;
            colorStopwatch.Reset();

            bodyStopwatch.Stop();
            bodyStopwatch.Reset();


        }

        public void DisposeStreams()
        {

            //colorFrameReader.Dispose();
            reader.Dispose();
        }

        //case in which window is closed without hitting stop
        public void StopListeningToAllStreams()
        {
            StopRecording();
            DisposeStreams();


        }

        public void RemoveAllStreamLists()
        {

            imageList.Clear();
        }



        public KinectSensor GetKinectSensor()
        {
            return kinectSensor;
        }

        void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();
            //if (reference.DepthFrameReference.AcquireFrame() != null && reference.BodyFrameReference.AcquireFrame() != null)
            //{
            using (var frame = reference.DepthFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    //Console.WriteLine("depth frame relative time: " + frame.RelativeTime);
                    TimeSpan time = DateTime.Now.TimeOfDay;
                    depthPixelsUShortArray = new ushort[frame.FrameDescription.Width * frame.FrameDescription.Height];
                    frame.CopyFrameDataToArray(depthPixelsUShortArray);
                    depthPixelsByteArray = EncodeDepthUShortArraytoByteArray(depthPixelsUShortArray, frame.DepthMaxReliableDistance);
                    Bitmap depthBitmap = ByteArrayToBitmap(depthPixelsByteArray, frame.FrameDescription);
                    depthBitmapsAndTimeStampsList.Add(Tuple.Create(depthBitmap, time));

                }
            }

            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    if (bodyframeList.Count == 0)
                    {
                        bodyStopwatch.Start();
                    }
                    //Console.WriteLine("body frame arrived: " + frame.RelativeTime);
                    bodies = new Body[kinectSensor.BodyFrameSource.BodyCount];
                    frame.GetAndRefreshBodyData(bodies);
                    foreach (var body in bodies)
                    {
                        if (body.IsTracked)
                        {
                            bodyframeList.Add(body);
                        }
                    }


                }
            }

            /*using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if (frame == null)
                    return;
                if (imageList.Count == 0)
                {
                    colorStopwatch.Start();
                }
                //Console.WriteLine("color frame arrived: " + frame.RelativeTime);
                var width = frame.FrameDescription.Width;
                var heigth = frame.FrameDescription.Height;
                var data = new byte[width * heigth * System.Windows.Media.PixelFormats.Bgra32.BitsPerPixel / 8];
                frame.CopyConvertedFrameDataToArray(data, ColorImageFormat.Bgra);

                var bitmap = new Bitmap(width, heigth, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                var bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.WriteOnly,
                    bitmap.PixelFormat);
                Marshal.Copy(data, 0, bitmapData.Scan0, data.Length);
                bitmap.UnlockBits(bitmapData);
                //Image<Bgr, Byte> frameActualKinect = new Image<Bgr, byte>(bitmap);

                imageList.Add(bitmap);
            }*/


        }

        //convert byte arrays to bitmaps before storing, so that they can easily be converted to images after
        private Bitmap ByteArrayToBitmap(Byte[] byteArray, FrameDescription frameDescription)
        {
            var bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel) / 8;
            var stride = bytesPerPixel * frameDescription.Width;

            BitmapSource bmpSource = BitmapSource.Create(frameDescription.Width, frameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null, byteArray, stride);
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bmpSource));
                enc.Save(outStream);
                Bitmap bitmap = new Bitmap(outStream);
                return bitmap;
            }

        }

        //technqiue used for encoding depth ushort to a byte array
        private byte[] EncodeDepthUShortArraytoByteArray(ushort[] ushortArray, ushort maxReliableDepth)
        {

            int numOfPixels = ushortArray.Count();
            byte[] byteArray = new byte[numOfPixels * 4];
            int colourIndex = 0;
            for (int i = 0; i < numOfPixels; i++)
            {
                ushort depth1 = ushortArray[i]; //last 3 bits of depth data are body ID
                if (depth1 <= maxReliableDepth)
                {
                    ushort red = (ushort)(depth1 >> 12);
                    ushort blue = (ushort)((ushort)(depth1 << 4) >> 12);
                    ushort green = (ushort)((ushort)(depth1 << 8) >> 11);
                    byteArray[colourIndex++] = (byte)(blue * 8);   //blue
                    byteArray[colourIndex++] = (byte)(green * 8);   //green
                    byteArray[colourIndex++] = (byte)(red * 8); //red
                    colourIndex++;
                }
                else
                {
                    byteArray[colourIndex++] = 0;   //blue
                    byteArray[colourIndex++] = 0;   //green
                    byteArray[colourIndex++] = 255; //red
                    colourIndex++;
                }
            }
            return byteArray;

        }

        //void FrameReader_FrameArrived(object sender, ColorFrameArrivedEventArgs e)
        //{

        //}



    }
}
