﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Accord.Video.FFMPEG;
using System.Threading;

namespace KinectTest
{

    public class ObjectToWrite
    {
        Bitmap bitmap;
        int index = 0;
        string file = "";
        int width = 0;
        int height = 0;
        int framerate = 0;
        string prefix = "";
        string folder = "";

        public ObjectToWrite(Bitmap bmp, int index, String file, int width, int height, int framerate, string prefix, string folder)
        {
            bitmap = bmp;
            this.index = index;
            this.file = file;
            this.width = width;
            this.height = height;
            this.framerate = framerate;
            this.prefix = prefix;
            this.folder = folder;
        }

        public void Writing(Object fileToWrite)
        {
            //Console.WriteLine("thread started...");
            Image<Bgr, byte> img = new Image<Bgr, Byte>(bitmap);
            bitmap.Save(folder + prefix + index + ".png");
            //using (VideoFileWriter writer = new VideoFileWriter())
            //{
            //    writer.Open(file, width, height, framerate, VideoCodec.Raw);
            //    writer.WriteVideoFrame(bitmap);
            //}

            //Console.WriteLine("thread ended...");
        }
    }
    public class VideoManager
    {
        string prefix = "test-1-";
        string colorFolder = "D:\\Tina\\Kinect Data\\testImg\\";
        string depthFolder = "D:\\Tina\\Kinect Data\\testDepth\\";

        public void WriteBitmapAndTimeStampsListToFile(List<Bitmap> bitmapList, string fileToOpen, TimeSpan timeLapsed)
        {
            //VideoFileReader reader 
            int bitmapListLength = bitmapList.Count();
            if (bitmapListLength != 0)
            {
                int frameRate = CalculateFrameRate(timeLapsed, bitmapListLength);

                int width = bitmapList[0].Width;
                int height = bitmapList[0].Height;
                //VideoWriter writer = new VideoWriter(fileToOpen, frameRate, width, height, true);

                for (int i = 0; i < bitmapListLength; i++)
                {
                    ObjectToWrite obj = new ObjectToWrite(bitmapList.ElementAt(i), i, fileToOpen, width, height, frameRate, prefix, colorFolder);
                    ThreadPool.QueueUserWorkItem(obj.Writing, obj);
                }

                Console.WriteLine("Completed writing to: " + fileToOpen + " using " + frameRate + " fps");

            }
        }




        public void WriteBitmapAndTimeStampsListToFile(List<Tuple<Bitmap, TimeSpan>> bitmapAndTimeStampList, string fileToOpen, TimeSpan timeLapsed, int bodyFrameCount)
        {
            int bitmapListLength = bitmapAndTimeStampList.Count();
            int frameRate = CalculateFrameRate(timeLapsed, bitmapListLength);

            int width = bitmapAndTimeStampList[0].Item1.Width;
            int height = bitmapAndTimeStampList[0].Item1.Height;
            VideoWriter writer = new VideoWriter(fileToOpen, frameRate, width, height, true);


            if (bitmapListLength != 0)
            {
                for (int i = 0; i < bitmapListLength; i++)
                {
                    if (bodyFrameCount != bitmapListLength && i != 0)
                    {
                        Image<Bgr, byte> img = new Image<Bgr, Byte>(bitmapAndTimeStampList[i].Item1);
                        img.Save( depthFolder + prefix + (i) + ".png");
                        writer.WriteFrame(img);
                    }


                }
                Console.WriteLine("Completed writing to: " + fileToOpen + " using " + frameRate + " fps");
                writer.Dispose();
            }




        }


        public int CalculateFrameRate(TimeSpan span, int numberOfFrames)
        {
            return (int)Math.Round(numberOfFrames / (span).TotalSeconds, 2);
        }
    }
}
