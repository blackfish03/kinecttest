﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Kinect;
using System.IO;

namespace KinectTest
{
    public partial class Form1 : Form
    {
        public KinectManager km = new KinectManager();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("enter start to begin recording:");

            //km.BodyFrameArrivedEvent += OnBodyFrameArrivedEventHandler;
            bool ok = km.LoadKinectSensor();
            Console.WriteLine("kinect status: " + ok);
        }

        private void start_Click_1(object sender, EventArgs e)
        {
            Console.WriteLine("start video");
            km.StartRecordingStreams();
        }

        private void stop_Click_1(object sender, EventArgs e)
        {
            km.StopRecording();
            VideoManager vm = new VideoManager();
            Stopwatch sw = new Stopwatch();

            //lock (colorBitmapAndTimeStampList);
            string fileToOpenColor = "D:\\Tina\\Kinect Data\\test_color.avi";
            string depthFile = "D:\\Tina\\Kinect Data\\test_depth.avi";
            Console.WriteLine("Writing to: " + fileToOpenColor);
            TimeSpan depthSpan =
                km.depthBitmapsAndTimeStampsList[(km.depthBitmapsAndTimeStampsList.Count() - 1)].Item2 - km.depthBitmapsAndTimeStampsList[0].Item2;
            String bodyFile = "D:\\Tina\\Kinect Data\\test_body.csv";
            sw.Start();
            Console.WriteLine("km.span: " + km.span);
            //vm.WriteBitmapAndTimeStampsListToFile(km.imageList, fileToOpenColor, km.span);
            vm.WriteBitmapAndTimeStampsListToFile(km.depthBitmapsAndTimeStampsList, depthFile, depthSpan, km.bodyframeList.Count());
            WriteBodyFrames(km.bodyframeList, bodyFile);
            sw.Stop();
            Console.WriteLine("Image list length: " + km.imageList.Count());
            Console.WriteLine("depth list length: " + km.depthBitmapsAndTimeStampsList.Count());
            Console.WriteLine("body list length: " + km.bodyframeList.Count());
            Console.WriteLine("disk writing time: " + sw.ElapsedMilliseconds);

            km.imageList.Clear();

            km.DisposeStreams();

        }

        private void WriteBodyFrames(List<Body> bodyframeList, string bodyFile)
        {

            String lines = "";
            for (int i = 0; i < bodyframeList.Count(); i++)
            {
                Body body = bodyframeList.ElementAt(i);
                foreach (var joint in body.Joints)
                {
                    lines += joint.Value.Position.X + "," + joint.Value.Position.Y + "," + joint.Value.Position.Z + ",";
                }
                lines += "\r\n";
            }

            File.WriteAllText(bodyFile, lines);
        }

        private void Window_Closed(object sender, FormClosedEventArgs e)
        {
            km.StopListeningToAllStreams();
            km.CloseKinectSensor();

        }
    }
}
