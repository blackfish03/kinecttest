# README #

This project serves 2 purposes:
	(1) To test whether the MultiSourceFrameReader class gives synchronized data streams between the color, depth, and body frame data
	(2) To prepare data for training the deep learning algorithm


### How do I get set up? ###

* How to run: Clone this repository in to your local machine.
* Build Configuration: VS 2015, Debug, x64.
* Dependencies: Ensure that Kinect SDK v2 is installed.

### How do I process data? ###

* Once you run the program and have a set of test images and the corresponding body frame data saved, go to the Matlab folder in this repository.
* Copy the test_body.csv data into the matlab folder
* Run PlotPointsnProcessData.m to get the 15 joints needed for DL (in the order of head, neck, spine, lShoulder, lElbow, lHand, rShoulder, rElbow, rHand, lHip, lKnee, lAnkle, rHip, rKnee, rAnkle)
* The script also appends a 1 at the end (not sure what this is; but from the DL example it just has this number attached to the end)
* Finally, in the same Matlab folder, open cmd and type `activate python 35`
* Run ` python addNames.py`
* The script will append a list of file names. 
* Change the starting index when appropriate

### Other Comments ###

* Change line 52 - 54 in VideoManager.cs to change the file prefix and file location (make sure you create the directory yourself!)

* Currently the program is set up for only depth and body frame data. To obtain color images follow the steps below
	1. Add FrameSourceTypes.Color to line 53 in KinectManager.cs
	2. Uncomment lines 151-175 in KinectManager.cs
	3. Uncomment line 55 in Form1.cs
	
* If you run the program in debug, it tells you the length of the image, depth, and body list. Usually when the depth and body list lengths match perfectly, nothing gets saved.